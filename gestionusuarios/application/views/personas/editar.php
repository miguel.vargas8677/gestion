<br>
<center>


  <h2 class="text-primary">ACTUALIZAR LA LISTA MEDICOS:</h2>
</center>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/personas/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_per" id="id_per"
   value="<?php echo $persona->id_per; ?>">
    <br>
    <br>

    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"
    value="<?php echo $persona->identificacion_per; ?>"
    type="number" name="identificacion_per"
    id="identificacion_per"
    placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control" value="<?php echo $persona->apellido_per; ?>" type="text" name="apellido_per" id="apellido_per" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $persona->nombre_per; ?>"   type="text" name="nombre_per" id="nombre_per" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $persona->telefono_per; ?>"    type="number" name="telefono_per" id="telefono_per" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control" value="<?php echo $persona->direccion_per; ?>"    type="text" name="direccion_per" id="direccion_per" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control" value="<?php echo $persona->email_per; ?>"    type="email" name="email_per" id="email_per" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">ESTADO</label>
    <select class="form-control" name="estado_per" id="estado_per">
        <option value="">--Seleccione--</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
<br>
<br>
    <label for="">FOTOGRAFIA</label>
      <input  class="form-control"  value="<?php echo $persona->foto_per; ?>"  type="file" name="foto_per"
    accept="image/*"
    id="foto_per"   >
<br>
    <br>
  <center>  <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/personas/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a></center>
    <br>
    <br>

</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#fk_id_pais")
   .val("<?php echo $cliente->fk_id_pais; ?>");

   $("#estado_cli")
   .val("<?php echo $cliente->estado_cli; ?>");

</script>
