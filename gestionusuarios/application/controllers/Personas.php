<?php
    class Personas extends CI_Controller{
      public function __construct(){
        parent:: __construct();
        $this->load->model("persona");

      }

      public function index(){
        $data["listadoPersonas"]=$this->persona->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("personas/index",$data);
        $this->load-> view("footer");
      }
      public function nuevo(){
        $data["listadoPersonas"]=$this->persona->consultarTodos();
        $this->load-> view("header");
        $this->load-> view("personas/nuevo",$data);
        $this->load-> view("footer");
      }
      public function editar($id_per){
        $data["listadoPersonas"]=$this->persona->consultarTodos();
        $data["persona"]=$this->persona->consultarPorId($id_per);
        $this->load-> view("header");
        $this->load-> view("personas/editar",$data);
        $this->load-> view("footer");
      }

      public function procesarActualizacion(){
        $id_per=$this->input->post("id_per");
        $datosPersonaEditado=array(
          "identificacion_per"=>$this->input->post("identificacion_per"),
          "apellido_per"=>$this->input->post("apellido_per"),
          "nombre_per"=>$this->input->post("nombre_per"),
          "telefono_per"=>$this->input->post("telefono_per"),
          "direccion_per"=>$this->input->post("direccion_per"),
            "email_per"=>$this->input->post("email_per"),
            "estado_per"=>$this->input->post("estado_per")

        );

      if ($this->persona->actualizar($id_per,$datosPersonaEditado)) {
        // echo "INSERCION EXITOSA";
        redirect("personas/index");
      }
      else {
        echo "ERROR AL INSERTAR";
            }
          }


      public function guardarPersona(){
        $datosNuevoPersona=array(
          "identificacion_per"=>$this->input->post("identificacion_per"),
          "apellido_per"=>$this->input->post("apellido_per"),
          "nombre_per"=>$this->input->post("nombre_per"),
          "telefono_per"=>$this->input->post("telefono_per"),
          "direccion_per"=>$this->input->post("direccion_per"),
            "email_per"=>$this->input->post("email_per"),
            "estado_per"=>$this->input->post("estado_per")
        );
        //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente
          $this->load->library("upload");//carga de la libreria de subida de archivos
          $nombreTemporal="foto_persona_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024; //2MB
          $this->upload->initialize($config);
          //codigo para subir el archivo y guardar el nombre en la BDD
          if($this->upload->do_upload("foto_per")){
            $dataSubida=$this->upload->data();
            $datosNuevoPersona["foto_per"]=$dataSubida["file_name"];
          }

      if ($this->persona->insertar($datosNuevoPersona)) {
        // echo "INSERCION EXITOSA";
        $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
      }
      else {
        $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
      }
      redirect("personas/index");
    }

    // FUNCIÒN PARA PROCESAR LA ELIMINACIÒN
    public function procesarEliminacion($id_per){
if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR") {
      if($this->medico->eliminar($id_per)){
        redirect("personas/index");
      }else{
        echo "ERROR AL ELIMINAR";
      }
    } else {
          redirect("seguridades/formularioLogin");
        }
    }

} //Cierre de la clase
?>
