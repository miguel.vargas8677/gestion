<?php
    class Seguridades extends CI_Controller{
      public function __construct(){
          parent::__construct();
          $this->load->model("usuario");
      }
      //funcion que se encarga de renderizar
      //la vista con el formulario de login
      public function formularioLogin(){
        $this->load->view("seguridades/formularioLogin");
      }
      //funcion que valida las credenciales ingresadas
      public function validarAcceso(){
          $email_usu=$this->input->post("email_usu");
          $password_usu=$this->input->post("password_usu");
          $usuario=
          $this->usuario->buscarUsuarioPorEmailPassword($email_usu,
                          $password_usu);
          if($usuario){
              if($usuario->estado_usu>0){//validando estado
                 //Creando la variable de sesion con el nombre  c0nectadoUTC
                  $this->session->set_userdata("c0nectadoUTC",$usuario);
                  $this->session->set_flashdata("bienvenida","Saludos, bienvenido al sistema");
                  redirect("clientes/index");//la primera vista que vera el usuario
              }else{
                $this->session->set_flashdata("error","Usuario bloqueado");
                redirect("seguridades/formularioLogin");
              }
          }else{//cuando no existe
            $this->session->set_flashdata("error","Email o contraseña iconrrectos");
            redirect("seguridades/formularioLogin");
          }
      }

      public function cerrarSesion(){
        $this->session->sess_destroy();//Matando la sesiones
        redirect("seguridades/formularioLogin");
      }

//http://localhost/ventas_22_22/index.php/seguridades/pruebaEmail   luis.quisaguano1@utc.edu.ec
public function pruebaEmail(){
enviarEmail("vmiguel038@gmail.com",
"PRUEBA", "<h1>HOLA</h1><i>Miguel Angel</i>");

}
//Para recuperar contraseña
public function recuperarPassword(){
$email=$this->input->post("email");
$password_aleatorio=rand(111111,999999);
$asunto="RECUPERAR PASSWORD";
$contendio="Su contraseña personal es: <b>$password_aleatorio</b>";

enviarEmail($email,$asunto,$contenido);
$this->session->set_flashdata("confirmacion",
"Hemos enviado una clave temporal a su doreccion de email");
redirect("seguridades/formularioLogin");



}
    }//Cierre de la clase
