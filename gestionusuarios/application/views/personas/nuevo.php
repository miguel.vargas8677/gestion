<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/personas/guardarPersona"
  method="post"
  id="frm_nuevo_persona"
  enctype="multipart/form-data"
  >


      <label for="">IDENTIFICACIÓN</label>
      <input class="form-control"  type="number" name="identificacion_per" id="identificacion_per" placeholder="Por favor Ingrese la Identificacion">
      <br>
      <br>
      <label for="">APELLIDO</label>
      <input class="form-control"  type="text" name="apellido_per" id="apellido_per" placeholder="Por favor Ingrese el apellido">
      <br>
      <br>
      <label for="">NOMBRE</label>
      <input class="form-control"  type="text" name="nombre_per" id="nombre_per" placeholder="Por favor Ingrese el nombre">
      <br>
      <br>

      <label for="">TELEFONO</label>
      <input class="form-control"  type="number" name="telefono_per" id="telefono_per" placeholder="Por favor Ingrese el télefono">
      <br>
      <br>
      <label for="">DIRECCIÓN</label>
      <input class="form-control"  type="text" name="direccion_per" id="direccion_per" placeholder="Por favor Ingrese la dirección">
      <br>
      <br>
      <label for="">CORREO ELECTRÓNICO</label>
      <input class="form-control"  type="email" name="email_per" id="email_per" placeholder="Por favor Ingrese el correo">
      <br>
      <br>
      <label for="">ESTADO</label>
      <select class="form-control" name="estado_per" id="estado_per">
          <option value="">--Seleccione--</option>
          <option value="ACTIVO">ACTIVO</option>
          <option value="INACTIVO">INACTIVO</option>
      </select>
      <br>
      <br>
      <label for="">FOTOGRAFIA</label>
      <input type="file" name="foto_per"
      accept="image/*"
      id="foto_per"  value="" >
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/personas/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a>

</form>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_per:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_per:{
          letras:true,
          required:true
        }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_per:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_per:{
          letras:"Apellido Incorrecto",
          required:"Por favor ingrese el apellido"
        }
      }
    });
</script>

<script type="text/javascript">
      $("#foto_cli").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true
      });
</script>









<!--  -->
