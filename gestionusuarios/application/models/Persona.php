<?php
    class Persona extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("persona",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_per,$datos){
        $this->db->where("id_per",$id_per);
        return $this->db->update("persona",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_per){
        $this->db->where("id_per",$id_per);

        $persona=$this->db->get("persona");
        if($persona->num_rows()>0){
          return $persona->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){

          $listadoPersonas=$this->db->get("persona");
          if($listadoPersonas->num_rows()>0){
            return $listadoPersonas;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_per){
        $this->db->where("id_per",$id_per);
        return $this->db->delete("medico");
      }


   }//cierre de la clase



   //
 ?>
